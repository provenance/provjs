# ProvJS Library #

This repository is for a Javascript implementation of the [W3C PROV standards](http://www.w3.org/TR/prov-overview/) developed by the [Provenance Research Team](https://provenance.ecs.soton.ac.uk/) at the [University of Southampton](http://www.southampton.ac.uk/).

Note that this library is still in development and is not finished yet.

See [primer-node.js](https://bitbucket.org/provenance/provjs/src/4e835f773d5f26a2a88a4083fd41c0a3c7f10940/primer-node.js?at=master) for the encoding the [W3 PROV Primer](http://www.w3.org/TR/prov-primer/) example to see what the library does.